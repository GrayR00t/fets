import os
import sys
import pandas as pd
import pytest
from sklearn.pipeline import Pipeline
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.date import TSNumericalToDate


def test_inits():
    tr_a = TSNumericalToDate()
    tr_b = TSNumericalToDate(past_limit="2001-01-01")
    tr_c = TSNumericalToDate(future_limit="2101-01-02")
    tr_d = TSNumericalToDate(fill_date="2102-01-02")

    assert tr_a.future_limit is None
    assert tr_a.past_limit is None
    assert tr_b.future_limit is None
    assert tr_b.past_limit == pd.to_datetime("2001-01-01 00:00:00")
    assert tr_c.future_limit == pd.to_datetime("2101-01-02 00:00:00")
    assert tr_c.past_limit is None
    assert tr_d.fill_date == pd.to_datetime("2102-01-02 00:00:00")


def test_no_limits():
    data_list = [2014, 201409.0, 23, 20130618.0, -99]
    test_ts = pd.Series(data_list)

    pipeline = Pipeline([
        ("to_datetime", TSNumericalToDate())
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 5
    assert result[0] == pd.to_datetime("2014-01-01")
    assert result[1] == pd.to_datetime("2014-09-01")
    assert result[2] is pd.NaT
    assert result[3] == pd.to_datetime("2013-06-18")
    assert result[4] is pd.NaT


def test_with_limits():

    data_list = [1998, 201409.0, 23, 20330618.0, -99]
    test_ts = pd.Series(data_list)

    pipeline = Pipeline([
        ("to_datetime", TSNumericalToDate("2014-01-02", "2018-06-18"))
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 5
    assert result[0] == pd.to_datetime("2014-01-02")
    assert result[1] == pd.to_datetime("2014-09-01")
    assert result[2] is pd.NaT
    assert result[3] == pd.to_datetime("2018-06-18")
    assert result[4] is pd.NaT


def test_with_bad_limits():

    data_list = [1998, 201409.0, 23, 20330618.0, -99]
    test_ts = pd.Series(data_list)

    with pytest.raises(ValueError) as excinfo:
        pipeline = Pipeline([
            ("to_datetime", TSNumericalToDate("20A4-0C-0D", "2018-06-18"))
        ])

        result = pipeline.transform(test_ts)
    assert "Unknown string format" in str(excinfo.value)

def test_unused_fit():

    
    data_list = [1998, 201409.0, 23, 20330618.0, -99]
    test_ts = pd.Series(data_list)

    pipeline = Pipeline([
        ("to_datetime", TSNumericalToDate("2014-01-02", "2018-06-18"))
    ])

    result = pipeline.fit(test_ts)

    # Nothing happened 
    assert isinstance(result, Pipeline)
