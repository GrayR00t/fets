import os
import sys
import pandas as pd
from sklearn.pipeline import Pipeline
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.selection import DFColumSelect


def test_selection():
    time_index = pd.date_range("2025-01-01 03:55:00", periods=5, freq="5H")
    test_df = pd.DataFrame({
                    "A":["PRE_ONE", "PRE_TWO", "POST_B", "POST_C", "PRE_TWO"],
                    "B":[4, 123, 24.2, 3.14, 1.41]
                    })

    assert len(time_index) == 5
    
    pipeline = Pipeline([
        ("col", DFColumSelect("B") )
    ])

    result = pipeline.transform(test_df)

    assert isinstance(result, pd.Series)
    assert len(result.shape) == 1
    assert result.shape[0] == 5
    assert result[0] == 4
    assert result[1] == 123
    assert result[4] == 1.41


def test_fit():
    tr_a = DFColumSelect()
    tr_b = DFColumSelect()
    tr_c = tr_a.fit(None)

    assert tr_a != tr_b
    assert tr_a is tr_c


def test_no_column():
    test_df = pd.DataFrame({
                    "A":["PRE_ONE", "PRE_TWO", "POST_B", "POST_C", "PRE_TWO"],
                    "B":[4, 123, 24.2, 3.14, 1.41]
                    })

    pipeline = Pipeline([
        ("col", DFColumSelect("C") )
    ])

    result = pipeline.transform(test_df)

    assert result.name == "A"
    assert result[0] == "PRE_ONE"
    assert result.shape[0] == 5

