import os
import sys
import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.math import TSPolynomialAB, TSScale, TSInterpolation, TSIntegrale


def test_polynomial():
    S = pd.Series([0, 0.3, 0.7, 1.0])

    pipeline = Pipeline([
        ("polynomial", TSPolynomialAB())
    ])

    result = pipeline.transform(S)

    assert result.shape[0] == 4
    assert result[0] == 0.0
    assert result[1] == 0.2601
    assert result[2] < 0.82810
    assert result[2] > 0.82809
    assert result[3] == 1.0

    pipeline = Pipeline([
        ("polynomial", TSPolynomialAB(power_b=3))
    ])

    result = pipeline.transform(S)
    assert result.shape[0] == 4
    assert result[0] == 0.0
    assert result[1] < 0.13265101
    assert result[1] > 0.132651
    assert result[2] < 0.7535710
    assert result[2] > 0.7535709
    assert result[3] == 1.0


def test_scale():
    S = pd.Series([0, 4, 3, 8.0])

    pipeline = Pipeline([
        ("norm", TSScale(scale_min=5,  scale_max=12))
    ])

    result = pipeline.transform(S)
    assert result.shape[0] == 4
    assert result[0] == 5.0
    assert result[1] == 8.5 
    assert result[2] == 7.625
    assert result[3] == 12.0


def test_scale_fit():
    S = pd.Series([0, 4, 3, 8.0])

    pipeline = Pipeline([
        ("norm", TSScale(scale_min=5,  scale_max=12))
    ])

    result = pipeline.fit(S)
    assert isinstance(result, Pipeline)


def test_scale_different_input():
    S = pd.Series([0, 4, 3, 8.0])

    pipeline = Pipeline([
        ("norm", TSScale(bound_min=4, bound_max=4, scale_min=5,  scale_max=12))
    ])

    result = pipeline.transform(S)
    assert result.shape[0] == 4
    assert result[0] == 5.0
    assert result[1] == 8.5 
    assert result[2] == 7.625
    assert result[3] == 12.0


def test_scale_bad_call():
    pipeline = Pipeline([
        ("norm", TSScale(scale_min=5,  scale_max=12))
    ])
    bad_result = pipeline.transform(["not", "a", "series"])
    assert isinstance(bad_result, list)
    assert bad_result[0] == "not"
    assert bad_result[1] == "a"
    assert bad_result[2] == "series"


def test_interpolation():
    S = pd.Series([0, 4, 3, 8.0])
    S.index = pd.date_range("2018-12-01 00:00:00", "2018-12-04 00:00:00", freq="D")
    new_index = pd.date_range("2018-12-01 00:00:00", "2018-12-04 00:00:00", freq="12h")

    pipeline = Pipeline([
        ("norm", TSInterpolation(new_index=new_index))
    ])

    result = pipeline.transform(S)

    assert result.shape[0] == 7
    assert result[0] == 0.0
    assert result[1] == 2.0 
    assert result[2] == 4.0
    assert result[3] == 3.5
    assert result[4] == 3.0
    assert result[5] == 5.5
    assert result[6] == 8.0

    pipeline2 = Pipeline([
        ("norm_no_given_index", TSInterpolation(period="12h"))
    ])

    result2 = pipeline2.transform(S)

    assert result2.shape[0] == 7
    assert result2[0] == 0.0
    assert result2[1] == 2.0 
    assert result2[2] == 4.0
    assert result2[3] == 3.5
    assert result2[4] == 3.0
    assert result2[5] == 5.5
    assert result2[6] == 8.0

def test_interpolation_bad_call():

    S = pd.Series([np.nan, 4, 3, 8.0])
    S.index = pd.date_range("2018-12-01 00:00:00", "2018-12-04 00:00:00", freq="D")
    new_index = pd.date_range("2018-12-01 00:00:00", "2018-12-04 00:00:00", freq="12h")

    pipeline = Pipeline([
        ("norm", TSInterpolation(new_index=new_index))
    ])

    bad_result = pipeline.transform(["not", "a", "series"])
    assert isinstance(bad_result, list)
    assert bad_result[0] == "not"
    assert bad_result[1] == "a"
    assert bad_result[2] == "series"

    result = pipeline.transform(S)
    assert result.shape[0] == 7
    assert np.isnan(result[0])
    assert np.isnan(result[1])


def test_interpolation_fit():
    S = pd.Series([0, 4, 3, 8.0])

    pipeline = Pipeline([
        ("norm", TSInterpolation())
    ])

    result = pipeline.fit(S)
    assert isinstance(result, Pipeline)


def test_integration():
    S = pd.Series([0, 4, 1, 12, 5.5, 1.2, 3, 8.0])
    S.index = pd.date_range("2018-12-01 00:00:00", "2018-12-08 00:00:00", freq="D")

    pipeline = Pipeline([
        ("integrale", TSIntegrale("2D"))
    ])

    result = pipeline.transform(S)

    assert result.shape[0] == 8
    assert result.index[0] == pd.to_datetime("2018-12-01 00:00:00")
    assert result[0] == 0.0
    assert result[1] == 4.0 
    assert result[2] == 5.0
    assert result[3] == 17.0 
    assert result[4] == 18.5
    assert result[5] == 18.7
    assert result[6] == 9.7
    assert result[7] == 12.2

    pipeline2 = Pipeline([
        ("integrale", TSIntegrale("2D", period_offset="1D"))
    ])

    result2 = pipeline2.transform(S)

    assert result2.shape[0] == 8
    assert result.index[0] == pd.to_datetime("2018-12-01 00:00:00")
    assert result2[0] == 0.0
    #assert result2[1] == 0.0 
    #assert result2[2] == 4.0
    #assert result2[3] == 5.0 
    #assert result2[4] == 17.5
    #assert result2[5] == 18.5
    #assert result2[6] == 18.7
    #assert result2[7] == 9.7


def test_integration_bad_input():
    pipeline = Pipeline([
        ("integrale", TSIntegrale("2D"))
    ])

    bad_result = pipeline.transform(["not", "that", "series"])
    assert isinstance(bad_result, list)
    assert bad_result[0] == "not"
    assert bad_result[1] == "that"
    assert bad_result[2] == "series"


def test_integration_fit():
    S = pd.Series([0, 4, 3, 8.0])

    pipeline = Pipeline([
        ("norm", TSIntegrale("1D"))
    ])

    result = pipeline.fit(S)
    assert isinstance(result, Pipeline)


def test_integration_inits():
    tr_a = TSIntegrale(1)

    assert tr_a.period == "5min"


