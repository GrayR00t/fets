import os
import sys
import pandas as pd
from sklearn.pipeline import Pipeline
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.text import TSExtractPrefix, TSExtractSuffix, TSExtractText
from fets.counter import TSCount


# --- Test data (TODO fixture?!) with 25 elements
data_list = ["PRE_ONE", "PRE_TWO", "POST_B", "POST_C", "PRE_TWO", "MIXAT", "MIXHERE"]
for n in range(7):
    data_list.append("PRESHIFT")
for n in range(10):
    data_list.append("SHIFT")
data_list.append("PREBUILD")

test_ts = pd.Series(data_list)
test_ts.index = pd.date_range("2025-01-01 03:55:00", periods=25, freq="15min")


def test_base_text():
    """ Dummy test in case we have a bad input.
    """
    data = [1, 2, 5]
    pipeline = Pipeline([
        ("prefix", TSExtractText("OST"))
    ])
    result = pipeline.transform(data)

    assert len(result) == len(data)
    assert result[0] == 1
    assert result[1] == 2
    assert result[2] == 5


def test_text():
    pipeline = Pipeline([
        ("prefix", TSExtractText("OST"))
    ])
    result = pipeline.transform(test_ts)

    assert result.shape[0] == 2
    assert result.values[0] == "POST_B"
    assert result.values[1] == "POST_C"


def test_base_suffix():
    data = [1, 2, 5]

    pipeline = Pipeline([
        ("prefix", TSExtractPrefix("SHIFT"))
    ])
    result = pipeline.transform(data)

    assert len(result) == len(data)
    assert result[0] == 1
    assert result[2] == 5


def test_base_prefix():
    data = [1, 2, 5]

    pipeline = Pipeline([
        ("prefix", TSExtractPrefix("SHIFT"))
    ])
    result = pipeline.transform(data)

    assert len(result) == len(data)
    assert result[2] == 5


def test_prefix():
    pipeline = Pipeline([
        ("prefix", TSExtractPrefix("SHIFT"))
    ])

    result = pipeline.transform(test_ts)
    assert result.name == "f0_SHIFT"
    assert result.shape[0] == 10

    pipeline2 = Pipeline([
        ("prefix", TSExtractPrefix(["PRE", "POST"]))
    ])

    result = pipeline2.transform(test_ts)
    assert result.name == "f0_PRE_POST"
    assert result.shape[0] == 13


def test_count_prefix():
    pipeline = Pipeline([
        ("prefix", TSExtractPrefix("PRE")),
        ("count", TSCount("60min"))
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 7
    assert result[0] == 1
    assert result[1] == 2
    assert result[2] == 2
    assert result[3] == 4
    assert result[4] == 1
    assert result[5] == 0
    assert result[6] == 1


def test_suffix():
    pipeline = Pipeline([
        ("suffix", TSExtractSuffix("SHIFT"))
    ])

    result = pipeline.transform(test_ts)
    assert result.name == "f0_SHIFT"
    assert result.shape[0] == 17


def test_count_suffix():
    pipeline = Pipeline([
        ("prefix", TSExtractSuffix("TWO")),
        ("count", TSCount("60min"))
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 1
    assert result[0] == 2
