import os
import sys
import pandas as pd
from sklearn.pipeline import Pipeline
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.counter import TSCount, TSTimerIncrement


def test_simple_count():
    time_index = pd.date_range("2025-01-01 03:55:00", periods=25, freq="15min")
    data_list = []
    for n in range(25):
        data_list.append("SAMPLE")

    test_ts = pd.Series(data_list, time_index)

    assert len(time_index) == 25
    assert test_ts.shape[0] == 25

    pipeline = Pipeline([
        ("count", TSCount("60min"))
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 7
    assert result[0] == 1
    assert result[1] == 4
    assert result[2] == 4
    assert result[3] == 4
    assert result[4] == 4
    assert result[5] == 4
    assert result[6] == 4


def test_timer_increment():
    test_ts = pd.Series(data=["START", "STOP", "STart", "stoptop"],
                        index=pd.date_range("2020-01-01 03:55:00", periods=4, freq="55min"))

    pipeline = Pipeline([
        ("timer", TSTimerIncrement("5min"))
    ])

    result = pipeline.transform(test_ts)
    assert isinstance(result, pd.Series)
    assert result.shape[0] == 34
    assert result.index[0] == pd.Timestamp("2020-01-01 03:55:00")
    assert result.max() == 50

    pipeline2 = Pipeline([
        ("timer", TSTimerIncrement("5min", cap=45))
    ])

    result = pipeline2.transform(test_ts)
    assert isinstance(result, pd.Series)
    assert result.shape[0] == 34
    assert result.max() == 45
