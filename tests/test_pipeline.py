import os
import sys
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
# from sklearn.preprocessing import FunctionTransformer
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.pipeline import FeatureUnion2DF


class Mult(BaseEstimator, TransformerMixin):
    """ Simple proper transformer for tests
        This one multiplies input by a given factor
    """
    def __init__(self, factor=1):
        self.factor = factor

    def transform(self, data, y=None, **tr_params):
        return data*self.factor


pipeline = Pipeline([
    ("union", FeatureUnion2DF([
        ("mult2", Mult(2)),
        ("mult3", Mult(3))
    ]))
])


def test_union_on_series():
    series = pd.Series([3, 2, 4, 2])
    result = pipeline.transform(series)

    assert isinstance(result, pd.DataFrame)
    assert result.shape[0] == 4
    assert result.shape[1] == 2
    assert result.columns[0] == "f0_mult2"
    assert result.columns[1] == "f1_mult3"

    assert result["f0_mult2"].at[0] == 6
    assert result["f0_mult2"].at[1] == 4
    assert result["f1_mult3"].at[0] == 9
    assert result["f1_mult3"].at[1] == 6


def test_union_on_dataframes():
    dframe = pd.DataFrame({"A":[3, 2, 4, 2]})
    result = pipeline.transform(dframe)

    assert isinstance(result, pd.DataFrame)
    assert result.shape[0] == 4
    assert result.shape[1] == 2
    assert result.columns[0] == "f_A_mult2"
    assert result.columns[1] == "f_A_mult3"

    assert result["f_A_mult2"].at[0] == 6
    assert result["f_A_mult2"].at[1] == 4
    assert result["f_A_mult3"].at[0] == 9
    assert result["f_A_mult3"].at[1] == 6

    dframe2 = pd.DataFrame({"A":[3, 2, 4, 2], "B":[1, 8, 5, 6]})
    result = pipeline.transform(dframe2)

    assert isinstance(result, pd.DataFrame)
    assert result.shape[0] == 4
    assert result.shape[1] == 4
    assert result.columns[0] == "f_A_mult2"
    assert result.columns[1] == "f_B_mult2"
    assert result.columns[2] == "f_A_mult3"
    assert result.columns[3] == "f_B_mult3"

    assert result["f_A_mult2"].at[0] == 6
    assert result["f_A_mult2"].at[1] == 4
    assert result["f_B_mult2"].at[0] == 2
    assert result["f_B_mult2"].at[1] == 16

    assert result["f_A_mult3"].at[0] == 9
    assert result["f_A_mult3"].at[1] == 6
    assert result["f_B_mult3"].at[0] == 3
    assert result["f_B_mult3"].at[1] == 24
