""" FETS setuptools-based setup.

Feature Engineering Transformer Set

"""
from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    version="0.5.1",
    name="fets",
    description="Feature Engineering Transformer Set",
    long_description=long_description,
    url="https://gitlab.com/redsharpbyte/fets",
    license="BSD",
    author="Red Boumghar",
    install_requires=["numpy", "scipy", "pandas", "scikit-learn", "joblib"],
    python_requires='>=3',
    extras_require={
        "test": ["pytest"]
    },
    packages=find_packages(exclude=["tests", "docs", "backends"]),
    keywords="data, feature-engineering, feature-extraction, machine-learning, transformer, transformation, pipeline",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License"
    ],
)
